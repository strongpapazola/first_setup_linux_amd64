from os import system
from urllib.request import urlopen

def internet_on():
    try:
        response = urlopen('https://www.google.com/', timeout=1)
        return True
    except:
        return False

def software():
    software = [
        "Arduino IDE":"https://downloads.arduino.cc/arduino-1.8.12-linux64.tar.xz"
    ]
    for i in software:
        confirm = input('Install '+str(i))
    
def installation():
    if internet_on() == False:
        print('Not Connect To Internet !!!')
        exit()
    else:
        system("apt update && apt upgrade && apt install openssh-server isc-dhcp-server bind9 apache2 php python3-pip wget w3m -y")
      
installation()
